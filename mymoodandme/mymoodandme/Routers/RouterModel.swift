//  RouterModel.swift

import UIKit

struct RouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
     var login: LoginRouterModel!
//     var home : HomeRouterModel!
//     var settings: SettingsRouterModel!
    
    init() {}
    
    init!(presenter: UINavigationController?) {
        
        guard let _presenter = presenter else {
            return nil
         }
        
        self.presenter = _presenter
        login = LoginRouterModel(with: _presenter)
    }
}

