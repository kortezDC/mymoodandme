//  StoryboardsRouter.swift

import UIKit

enum StoryboardsRouterList: String {
    case login = "Login"
    case home = "Home"
    case settings = "Settings"
}

protocol StoryboardsRouter {
    func boardLogin() -> UIStoryboard
    func boardHome() -> UIStoryboard
    func boardSettings() -> UIStoryboard
}

extension StoryboardsRouter {
    
    func boardLogin() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.login.rawValue, bundle: nil)
    }
    
    func boardHome() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.home.rawValue, bundle: nil)
    }

    func boardSettings() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.settings.rawValue, bundle: nil)
    }
}
