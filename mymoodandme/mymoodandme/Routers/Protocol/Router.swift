//  RouterStoryboards.swift
//  Spot

import UIKit

protocol Router {
    
    weak var presenter: UINavigationController? {get}
    func present(_ controller: UIViewController, animated: Bool, complate: (() -> ())?)
    func push(_ controller: UIViewController, animated: Bool)
    func popController(_ animated: Bool)
    func dismissController(_ animated: Bool, complate: (() -> ())?)
}

extension Router {
    
    func present(_ controller: UIViewController, animated: Bool = true, complate: (() -> ())? = nil) {
        presenter?.present(controller, animated: animated, completion: complate)
    }
    
    func push(_ controller: UIViewController, animated: Bool = true) {
        presenter?.pushViewController(controller, animated: animated)
    }
    
    func popController(_ animated: Bool = true) {
        _ = presenter?.popViewController(animated: animated)
    }
    
    func popRootController(_ animated: Bool = true) {
        _ = presenter?.popToRootViewController(animated: animated)
    }
    
    func dismissController(_ animated: Bool = true, complate: (() -> ())? = nil) {
        presenter?.dismiss(animated: animated, completion: complate)
    }
}
