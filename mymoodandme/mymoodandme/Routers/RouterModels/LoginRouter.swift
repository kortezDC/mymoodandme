//  LoginRouter.swift

import Foundation
import UIKit

enum LoginVC : String{
    case StartViewController
    case LoginViewController
    case PolicyViewController
    case SignUpViewController
}

struct LoginRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init() {}
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
        presenter?.isNavigationBarHidden = true
    }
    
    func presentStartController(_ animation: Bool = true) {
        if let startController = boardLogin().instantiateViewController(withIdentifier: LoginVC.StartViewController.rawValue) as? StartViewController {
            
            push(startController, animated: animation)
        }
    }
    
    func presentPolicyController(_ animation: Bool = true) {
        if let policyController = boardLogin().instantiateViewController(withIdentifier: LoginVC.PolicyViewController.rawValue) as? PolicyViewController {
            
            let policyNavigation = UINavigationController(rootViewController: policyController)
            present(policyNavigation, animated: animation)
        }
    }
    
    func presentSignUpController(_ animation: Bool = true) {
        if let signUpController = boardLogin().instantiateViewController(withIdentifier: LoginVC.SignUpViewController.rawValue) as? SignUpViewController {
            push(signUpController, animated: animation)
            presenter?.isNavigationBarHidden = false
        }
    }
}
