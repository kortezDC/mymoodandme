//
//  SignUpViewController.swift
//  mymoodandme
//
//  Created by Kortez on 13.03.17.
//  Copyright © 2017 Inteza. All rights reserved.
//

import Foundation
import UIKit

class SignUpViewController : BaseController {
    
    @IBAction func backButton(_ sender: Any) {
        router.presenter?.isNavigationBarHidden = true
        router.popController()
    }
}
