//
//  LoginViewController.swift
//  mymoodandme
//
//  Created by Kortez on 13.03.17.
//  Copyright © 2017 Inteza. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController : BaseController {
    
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    
    
    let viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        turnOnKeyboardNotifications()

        viewModel.delegate = self
    }
    
    @IBAction func signUpAction(_ sender: Any){
        router.login.presentSignUpController()
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any){
        print("forgot password")
    }
}

extension LoginViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField {
           passwordTextField.becomeFirstResponder()
        }else{
            view.endEditing(true)
            viewModel.loginAction()
        }
        return true
    }
}

extension LoginViewController : ViewModelDelegate {
    
    func viewModelDidStartUpdate() {
        // TODO
    }
    
    func viewModelDidEndUpdate() {
        // TODO
    }
}
