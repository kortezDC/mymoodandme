//  StartRouterProtocol.swift
//  mymoodandme
//
//  Created by Kortez on 13.03.17.
//  Copyright © 2017 Inteza. All rights reserved.
//

import Foundation
import UIKit

protocol StartRouterProtocol {
    func showLogin()
    func showStart()
    func showPolicy()
}

extension StartRouterProtocol where Self : StartViewController {
    
    func showLogin(){
        UIView.animate(withDuration: 0.4, animations: {
            self.containerViewStart.alpha = 0
            self.containerViewLogin.alpha = 1
            self.isLogin = true
        })
    }
    
    func showStart(){
        UIView.animate(withDuration: 0.4, animations: {
            self.containerViewStart.alpha = 1
            self.containerViewLogin.alpha = 0
            self.isLogin = false
        })
    }
    
    func showPolicy(){
        router.login.presentPolicyController()
    }
}
