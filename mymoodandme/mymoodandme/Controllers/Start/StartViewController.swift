//
//  ViewController.swift
//  mymoodandme
//
//  Created by Kortez on 13.03.17.
//  Copyright © 2017 Inteza. All rights reserved.
//

import UIKit

class StartViewController: BaseController, StartRouterProtocol {

    @IBOutlet weak var containerViewStart: UIView!
    @IBOutlet weak var containerViewLogin: UIView!
    @IBOutlet var hideLoginScene: UITapGestureRecognizer!
    
    
    var isLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isLogin = AppSeetings.shared.isFirstLaunch()
        if isLogin {
             showStart()
        }else{
            showLogin()
        }
    }
    
    @IBAction func hideLogin(_ sender: Any) {
        if isLogin {
            showStart()
            view.endEditing(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StartScene" {
            guard let containerViewController = segue.destination as? StartContainerView else {
                return
            }
            containerViewController.routerDelegate = self
        }
    }
}
