//
//  StartContainerView.swift
//  mymoodandme
//
//  Created by Kortez on 13.03.17.
//  Copyright © 2017 Inteza. All rights reserved.
//

import Foundation

class StartContainerView : BaseController {
    
    var routerDelegate : StartRouterProtocol? = nil
    
    @IBAction func enterAction(_ sender:Any){
        routerDelegate?.showLogin()
    }

    @IBAction func policyAction(_ sender:Any){
        routerDelegate?.showPolicy()
    }
}
