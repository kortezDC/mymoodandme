//  UserModel.swift

import Foundation
import ObjectMapper

class UserModel: NSObject, Mappable {
    
    var uid = ""
    var email = ""
    var username = ""
    var firstname = ""
    var lastname = ""
    var avatar = ""
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        username  <- map["username"]
        firstname <- map["firstname"]
        lastname  <- map["lastname"]
    }
    
    var getFullName : String{
        if firstname.isEmpty && lastname.isEmpty  {
            return email
        }else if !firstname.isEmpty && lastname.isEmpty{
            return firstname
        }else if !lastname.isEmpty && firstname.isEmpty{
            return lastname
        }
        return firstname + " " + lastname
    }
}
