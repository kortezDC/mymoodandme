//  BaseObjectModel.swift

//  Created by Kortez on 01.03.17.

import Foundation
import ObjectMapper

class BaseObjectModel : NSObject, Mappable{

    var timestamp = 0.0

    required override init() {
        super.init()
    }
    
    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {}
}
