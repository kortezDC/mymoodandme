//  ViewModel.swift

import UIKit

class ViewModel{

    let currentUser = UserManager.shared.getLoggedInUser
    var container = [BaseObjectModel]()
    
    /// Handles events that happen within the view model
    weak var delegate: ViewModelDelegate? {
        didSet {
            delegate?.viewModelDidStartUpdate()
        }
    }
    
    init() {
        /// TODO
    }
    
    /// Basic sort
    func sortContainer(){
        container.sort(by: { ($0).timestamp > ($1).timestamp })
    }
}
