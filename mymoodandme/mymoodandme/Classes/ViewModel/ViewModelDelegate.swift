///  ViewModelDelegate.swift

import Foundation

/// Delegate for handling changes to a view model
protocol ViewModelDelegate: class {
    
    /// Tells the delegate that the view model has been updated
    func viewModelDidStartUpdate()
    func viewModelDidEndUpdate()
}
