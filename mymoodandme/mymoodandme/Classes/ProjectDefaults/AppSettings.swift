//  AppSettings.swift

import Foundation


struct AppSeetings {
    
    let userDefault = UserDefaults.standard
    let userDefaultAccessToken = "UserDefaultAccessToken"
    let userDefaultAccessTokenExpiredAt = "UserDefaultAccessTokenExpiredAt"
    let userDefaultAccessDeviceToken = "UserDefaultAccessDeviceToken"
    
    var pageAllCount = 0
    var pageCurrent = 1
    var perPage = 50
    
    // MARK: - Singleton
    static var shared : AppSeetings = {
        let instance = AppSeetings()
        return instance
    }()
    
    func recordDeviceToken(token: String) {
       
        userDefault.set(token, forKey: userDefaultAccessDeviceToken)
        userDefault.synchronize()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeviceTokenUpdated"), object: nil)
    }
    
    func getDeviceToken() -> String? {
        
        if let token = userDefault.object(forKey: userDefaultAccessDeviceToken){
            return token as? String
        }else{
            // for simulator debug
            return nil
        }
    }
    
    func recordAccessToken(token: String, expiredAt: Int) -> Bool {
        
        userDefault.set(token, forKey: userDefaultAccessToken)
        userDefault.set(expiredAt, forKey: userDefaultAccessTokenExpiredAt)
        return userDefault.synchronize()
    }
    
    func recordAccessTokenExpiredAt(expiredAt: Int) {
        userDefault.set(expiredAt, forKey: userDefaultAccessTokenExpiredAt)
        userDefault.synchronize()
    }
    
    func getAccessToken() -> (token: String?, expiredAt: Int)? {
        if let token = userDefault.object(forKey: userDefaultAccessToken) as? String {
            return (token, userDefault.integer(forKey: userDefaultAccessTokenExpiredAt))
        } else {
            return nil
        }
    }
    
    func clearAccessToken() {

        userDefault.set(nil, forKey: userDefaultAccessToken)
        userDefault.set(0, forKey: userDefaultAccessTokenExpiredAt)
        userDefault.synchronize()
    }
    
    /// Check if is the first launch ever
    ///
    /// - Returns: true if first, false - app already launched
    func isFirstLaunch() -> Bool {
        if(userDefault.bool(forKey: "appFirstTimeOpend")){
            return false
        } else {
            userDefault.set(true, forKey: "appFirstTimeOpend")
            userDefault.synchronize()
            return true
        }
    }
    
    mutating func recordPaginationValues(current: Int, count: Int){
        pageCurrent = current
        pageAllCount = count
    }
    
    mutating func recordDefaultPaginationValues(){
        pageCurrent = 0
        pageAllCount = 1
    }
}
