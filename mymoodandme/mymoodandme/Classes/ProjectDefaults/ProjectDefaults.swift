//  ProjectDefaults.swift

import Foundation

enum PlistKeys : String {
    
    case apiURL
}

/// Project defaults and constants credentials

struct ProjectDefaults {
    
    /// project credentials
    func getPlistValue(valueFor key : PlistKeys)->String {
        if let val = Bundle.main.object(forInfoDictionaryKey: key.rawValue) as? String {
            return val
        }else{
            return "parameter: \(key.rawValue) - not set in info.plist"
        }
    }    
}
