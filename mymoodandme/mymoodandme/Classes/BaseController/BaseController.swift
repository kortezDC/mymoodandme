//  BaseController.swift

import UIKit

class BaseController: UIViewController {
    /// Keyboard protocol
    var keyboardYView : CGFloat = 0.0
    var keyboardHeightView : CGFloat = 0.0
    var keyboardFullKeyboard: Bool = false
    var keyboardDeltaHeightKeyboard: CGFloat = 0.0
    var addGesture: Bool = true
    lazy var tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.gestureTapOnView))
    var isShowKeyboard: Bool = false
    /// router
    lazy var router: RouterModel! = RouterModel(presenter: self.navigationController)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.keyboardYView = self.view.frame.origin.y
            self.keyboardHeightView = self.view.frame.size.height
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Actions
    func actionBackBarItem() {
        router.popController()
    }
    
    /// Add keyboard notifications
    func turnOnKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
