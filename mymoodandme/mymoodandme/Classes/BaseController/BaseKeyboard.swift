///  KeyboardModel.swift

import UIKit

/// subscribe
/// NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
/// NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

extension BaseController: Keyboard {
    
    func gestureTapOnView() {
        self.view.endEditing(true)
        self.view.removeGestureRecognizer(tapGesture)
    }

    /// MARK: Update View
    /// Show
    func showKeyboardUpdateView(_ aNotification:Notification) {
        
        guard let info = (aNotification as NSNotification).userInfo, let value = info[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        self.isShowKeyboard = true
        
        let duration : TimeInterval = {
            if self.view.frame.size.height == self.keyboardHeightView {
                return 0.35
            } else {
                return 0
            }
        } ()
        
        let keyboardFrame: CGRect = value.cgRectValue
        
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions.layoutSubviews, animations: {
            self.view.frame.size.height = self.keyboardHeightView - (keyboardFrame.size.height) - self.keyboardDeltaHeightKeyboard
        }) { (_) in
            if !self.addGesture { return }
            if let _ = self.view.gestureRecognizers?.index(where: { $0 == self.tapGesture}) {} else {
                self.view.addGestureRecognizer(self.tapGesture)
            }
        }
    }
    
    /// Hide
    func hideKeyboardUpdateView(_ aNotification:Notification) {
        
        if self.view.frame.origin.y == self.keyboardHeightView { return }
        
        self.isShowKeyboard = false
        
        UIView.animate(withDuration: 0.35, delay: 0, options: UIViewAnimationOptions.layoutSubviews, animations: {
            self.view.frame.size.height = self.keyboardHeightView
        }) { (_) in
            if let _ = self.view.gestureRecognizers?.index(where: { $0 == self.tapGesture}) {} else {
                self.view.removeGestureRecognizer(self.tapGesture)
            }
        }
    }
    
/// MARK: Up View
    
    /// Show
    func showKeyboardUpView(_ aNotification:Notification) {
        
        guard let info = (aNotification as NSNotification).userInfo, let value = info[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        self.isShowKeyboard = true
        
        let duration : TimeInterval = {
            if self.view.frame.origin.y == self.keyboardYView {
                return 0.35
            } else {
                return 0
            }
        } ()
        
        let keyboardFrame: CGRect = value.cgRectValue
        
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions.layoutSubviews, animations: {
            if self.keyboardFullKeyboard {
                self.view.frame.origin.y = self.keyboardYView - (keyboardFrame.size.height) - self.keyboardDeltaHeightKeyboard
                
            } else {
                self.view.frame.origin.y = self.keyboardYView - (keyboardFrame.size.height/2) - self.keyboardDeltaHeightKeyboard
            }
            
        }) { (_) in
            if !self.addGesture { return }
            if let _ = self.view.gestureRecognizers?.index(where: { $0 == self.tapGesture}) {} else {
                self.view.addGestureRecognizer(self.tapGesture)
            }
        }
    }
    
    /// Hide
    func hideKeyboardUpView(_ aNotification:Notification) {
        
        if self.view.frame.origin.y == self.keyboardYView { return }
        
        self.isShowKeyboard = false
        
        UIView.animate(withDuration: 0.35, delay: 0, options: UIViewAnimationOptions.layoutSubviews, animations: {
            self.view.frame.origin.y = self.keyboardYView
        }) { (_) in
            if let _ = self.view.gestureRecognizers?.index(where: { $0 == self.tapGesture}) {} else {
                self.view.removeGestureRecognizer(self.tapGesture)
            }
        }
    }
}
