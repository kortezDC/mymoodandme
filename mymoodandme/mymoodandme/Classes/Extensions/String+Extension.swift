//  String+Extension.swift

import Foundation

extension String {
    
    static func formateDate(created_at : Double) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        return formatter.string(from: Date(timeIntervalSince1970: (created_at)))
    }
}
