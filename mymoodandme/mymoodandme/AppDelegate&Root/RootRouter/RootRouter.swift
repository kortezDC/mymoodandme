//  RootRouter.swift
//  Spot

import UIKit
import NVActivityIndicatorView
import Fabric
import Crashlytics

/// RootRouter: main router for whole app
class RootRouter {
    
    /// Get top view controller
    var topViewController: UIViewController? {
        
        return UIApplication.topViewController()
    }
    
    /// window for navigation
    var window: UIWindow?
    
    // MARK: - Singleton
    static let shared: RootRouter = {
        
        let instance = RootRouter()
        return instance
    }()
    
    /// Custom didFinishLaunchingWithOptions method
    /// Tells the delegate that the launch process is almost done and the app is almost ready to run.
    ///
    /// - Parameters:
    ///   - launchOptions: A dictionary indicating the reason the app was launched
    ///   - window: The app's visible window
    /// - Returns: NO if the app cannot handle the URL resource or continue a user activity, otherwise return YES. The return value is ignored if the app is launched as a result of a remote notification.
    func application(didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?, window: UIWindow) -> Bool {
        
        /// window init
        RootRouter.shared.window = window
        RootRouter.shared.window?.makeKeyAndVisible()
        /// Initialize router logic
        startRootCoordinator()
        /// Setup Fabric
        Fabric.with([Crashlytics.self])
        
        return true
    }
    
    /// Setup launch navigation logic
    func startRootCoordinator() {
        /// If user already login - open dashboard
        if UserManager.shared.getLoggedInUser != nil {
            RootRouter.shared.window?.rootViewController = UIViewController()
        }else{
            guard let navController = RootRouter.shared.window?.rootViewController as? UINavigationController else {
                return
            }
            let rootCoordinator = LoginRouterModel(with: navController)
            rootCoordinator?.presentStartController()
        }
    }
}

/// Additional extension
extension UIApplication {
    
    /// Show progress hud view
    class func showHUD(){
        
        let activityData = ActivityData()
        hideActivity()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }

    /// Hide current representing hud view
    class func hideActivity(){
        
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }

    /// Get top viewcontroller from window hierarchy
    ///
    /// - Parameter base: root viewControlelr
    /// - Returns: current viewcontroller representing on screen
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = base as? UINavigationController {
            return topViewController(navigationController.visibleViewController)
        }
        if let tabBarController = base as? UITabBarController {
            if let selected = tabBarController.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presentedViewController = base?.presentedViewController {
            return topViewController(presentedViewController)
        }
        if base == nil {
            return UIApplication.shared.delegate?.window??.rootViewController
        }
        return base
    }
    
    /// Check if controller present like modal
    ///
    /// - Parameter viewController: controller for check
    /// - Returns: Bool value - is controller modal or not
    class func isModal(viewController: UIViewController) -> Bool {
        
        if viewController.presentingViewController != nil {
            return true
        } else if viewController.navigationController?.presentingViewController?.presentedViewController == viewController.navigationController  {
            return true
        } else if viewController.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
    
    /// show alert view
    ///
    /// - Parameter text: string message to represent
    class func showAlertMessage(message text: String) {
        
        let alert = UIAlertController(title: "Information", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        
        DispatchQueue.main.async(execute: {
            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
        })
    }
    
    /// show alert view with custom action
    ///
    /// - Parameters:
    ///   - text: string message to represent
    ///   - handler: trigger action by clicking on OK
    class func showAlertMessageWithAction(message text: String, handler: @escaping (UIAlertAction) -> Void) {
        
        let alert = UIAlertController(title: "Information", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: handler))
        
        DispatchQueue.main.async(execute: {
            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
        })
    }
}
